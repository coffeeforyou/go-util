package util

import (
	"fmt"
	"reflect"
)

func GetFieldString[T any](e *T, field string) string {
	r := reflect.ValueOf(e)
	f := reflect.Indirect(r).FieldByName(field)
	return f.String()
}

func GetFieldInteger[T any](e *T, field string) int {
	r := reflect.ValueOf(e)
	f := reflect.Indirect(r).FieldByName(field)
	return int(f.Int())
}

func Merge[T any](dst *T, src *T) error {
	srcVal := reflect.ValueOf(src)
	dstVal := reflect.ValueOf(dst)
	if srcVal.Elem().Kind() != reflect.Struct {
		return fmt.Errorf("merging received values failed, only structs supported")
	}
	srcFields := reflect.VisibleFields(reflect.TypeOf(*src))
	for _, field := range srcFields {
		if !field.IsExported() {
			continue
		}
		// Value to copy from src to dst
		cv := srcVal.Elem().FieldByIndex(field.Index)
		// Only handling pointers!
		if field.Type.Kind() == reflect.Pointer {
			if !cv.IsNil() {
				dstVal.Elem().FieldByIndex(field.Index).Set(cv)
			}
		} else {
			return fmt.Errorf("merging received values failed")
		}
	}
	return nil
}
