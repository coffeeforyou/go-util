package util

import (
	"fmt"
	"time"
)

// DateDifferenceInDays returns the difference in days (ignoring time) from t1 to t2
func DateDifferenceInDays(t1 time.Time, t2 time.Time) int {
	t1r := time.Date(t1.Year(), t1.Month(), t1.Day(), 0, 0, 0, 0, time.UTC)
	t2r := time.Date(t2.Year(), t2.Month(), t2.Day(), 0, 0, 0, 0, time.UTC)
	days := t2r.Sub(t1r).Hours() / 24
	return int(days)
}

// NewTime is a convenience function to create time.Time objects
func NewTime(year, month, day int) time.Time {
	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
}

// GetSimpleDateTimeString returns a date string in the format 02/01/2006 15:04
func GetSimpleDateTimeString() string {
	t := time.Now()
	return t.Format(SimpleDateTimeFormat)
}

// GetSimpleFilenameDateTimeString returns a date string in the format 2006-01-02-15-04-05
func GetSimpleFilenameDateTimeString() string {
	t := time.Now()
	return t.Format(SimpleDateTimeFilenameFormat)
}

const SimpleDateFormat = "02/01/2006"
const SimpleDateTimeFormat = "02/01/2006 15:04"
const SimpleDateTimeFilenameFormat = "2006-01-02-15-04-05"

// DateDifference returns the time difference between two given time.Time values
func DateDifference(a, b time.Time) (year, month, day, hour, min, sec int) {
	if a.Location() != b.Location() {
		b = b.In(a.Location())
	}
	if a.After(b) {
		a, b = b, a
	}
	y1, M1, d1 := a.Date()
	y2, M2, d2 := b.Date()

	h1, m1, s1 := a.Clock()
	h2, m2, s2 := b.Clock()

	year = int(y2 - y1)
	month = int(M2 - M1)
	day = int(d2 - d1)
	hour = int(h2 - h1)
	min = int(m2 - m1)
	sec = int(s2 - s1)

	// Normalize negative values
	if sec < 0 {
		sec += 60
		min--
	}
	if min < 0 {
		min += 60
		hour--
	}
	if hour < 0 {
		hour += 24
		day--
	}
	if day < 0 {
		// days in month:
		t := time.Date(y1, M1, 32, 0, 0, 0, 0, time.UTC)
		day += 32 - t.Day()
		month--
	}
	if month < 0 {
		month += 12
		year--
	}

	return year, month, day, hour, min, sec
}

// DateDifference returns the time difference between two given time.Time values
func DateDifferenceFormatted(a, b time.Time) string {
	res := make([]string, 0)
	var year, month, day, hour, min, sec int = DateDifference(a, b)
	vals := []int{year, month, day, hour, min, sec}
	keys := []string{"year", "month", "day", "hour", "minute", "second"}
	for i, v := range vals {
		if v > 1 {
			res = append(res, fmt.Sprintf("%d %ss", v, keys[i]))
		} else if v > 0 {
			res = append(res, fmt.Sprintf("%d %s", v, keys[i]))
		}
	}
	if len(res) == 0 {
		return "0 seconds"
	}
	return CombineStrings(res)
}
