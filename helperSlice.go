package util

import (
	"fmt"
	"strings"
)

// Generate a slice from a map
func SliceFromMap[K comparable, V any](m map[K]*V) []*V {
	res := make([]*V, len(m))
	i := 0
	for _, val := range m {
		res[i] = val
		i++
	}
	return res
}

// Generate a map from a slice with elements having an ID property
func SliceWithIdsToMap[V any](s []*V) map[int]*V {
	res := make(map[int]*V, len(s))
	i := 0
	for _, val := range s {
		id := GetFieldInteger(val, "Id")
		res[id] = val
		i++
	}
	return res
}

// Generate a slice of the IDs (ints) from a slice with elements having an ID property
func SliceWithIdsToSliceOfIds[V any](s []*V) []int {
	res := make([]int, len(s))
	for i, val := range s {
		id := GetFieldInteger(val, "Id")
		res[i] = id
	}
	return res
}

// Generate a slice from a map's keys (generic)
func SliceFromMapKeys[K comparable, V any](m map[K]*V) []K {
	res := make([]K, len(m))
	i := 0
	for k := range m {
		res[i] = k
		i++
	}
	return res
}

// Convert a slice of values to a slice of value pointers
func SliceWithPointersFromSliceWithValues[T any](values []T) []*T {
	res := make([]*T, len(values))
	for i := range values {
		res[i] = &values[i]
	}
	return res
}

// Convert a slice of value pointers to a slice of values
func SliceWithValuesFromSliceWithPointers[T any](values []*T) []T {
	res := make([]T, len(values))
	for i, v := range values {
		res[i] = *v
	}
	return res
}

// Removes item at position i from slice s
func SliceRemoveOneByIndex[T any](s []T, i int) ([]T, error) {
	l := len(s)
	if i > l || i < 0 {
		return s, fmt.Errorf("removing item from slice failed")
	}
	s[i] = s[l-1]
	res := s[:l-1]
	return res, nil
}

// Removes items at the given positions from slice s
func SliceRemoveManyByIndex[T comparable](s []T, indicesToRemove []int) ([]T, error) {
	ls := len(s)
	lr := len(indicesToRemove)
	if ls == lr {
		return make([]T, 0), nil
	}
	var zero T
	for _, itr := range indicesToRemove {
		if itr > ls || itr < 0 {
			return s, fmt.Errorf("removing item from slice failed")
		}
		s[itr] = zero
	}
	replacedCount := 0
	for replacedCount < lr && ls > 0 {
		if s[ls-1] != zero {
			s[indicesToRemove[replacedCount]] = s[ls-1]
			replacedCount++
		}
		ls--
	}
	res := s[:len(s)-len(indicesToRemove)]
	return res, nil
}

// Joins the elements of the slice with separator delim
func SliceJoinElems[T any](s []T, delim string) string {
	return strings.Join((strings.Split(strings.Trim(fmt.Sprint(s), "[]"), " ")), delim)
}

// Takes a slice and looks for an element in it. If found it will return its index, otherwise it will return -1 and a bool of false.
func SliceFindElement[T comparable](slice []T, val T) (int, bool) {
	for i, item := range slice {
		if item == val {
			return i, true
		}
	}
	return -1, false
}

// Takes a slice (slice) and removes all elements of the other slice (val), if found. Will return the new slice and the number of removed elements.
func SliceRemoveElements[T comparable](slice []T, val []T) ([]T, int) {
	res := make([]T, 0)
	count := 0
	for _, item := range slice {
		_, found := SliceFindElement(val, item)
		if !found {
			res = append(res, item)
		} else {
			count++
		}
	}
	return res, count
}

// Takes a slice (slice) and removes all duplicates. Will return the new slice.
func SliceRemoveDuplicates[T comparable](slice []T) []T {
	allKeys := make(map[T]bool)
	list := []T{}
	for _, item := range slice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}
