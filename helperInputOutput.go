package util

import (
	"archive/zip"
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
)

func ReadFileToString(fileName string) (*string, error) {
	// Read entire file content
	content, err := os.ReadFile(fileName)
	if err != nil {
		return nil, err
	}
	// Convert []byte to string and return
	text := string(content)
	return &text, nil
}

func CreateDir(dirName string) error {
	if _, err := os.Stat(dirName); errors.Is(err, os.ErrNotExist) {
		err := os.Mkdir(dirName, os.ModePerm)
		return err
	}
	return nil
}

func FileExists(fileName string) bool {
	if _, err := os.Stat(fileName); errors.Is(err, os.ErrNotExist) {
		return false
	}
	return true
}

func ZipSingleFile(name string, content []byte) (*bytes.Buffer, error) {
	// Create a buffer to write our archive to.
	buf := new(bytes.Buffer)
	// Create a new zip archive.
	w := zip.NewWriter(buf)
	// Add file to the archive.
	f, err := w.Create(name)
	if err != nil {
		return nil, err
	}
	_, err = f.Write(content)
	if err != nil {
		return nil, err
	}
	// Make sure to check the error on Close.
	err = w.Close()
	if err != nil {
		return nil, err
	}
	return buf, nil
}

func UnzipSingleFileArchive(file []byte) ([]byte, error) {
	// Create a ZIP reader to read the file
	br := bytes.NewReader(file)
	zr, err := zip.NewReader(br, br.Size())
	if err != nil {
		return nil, err
	}
	// Only archives with single file are processed
	if len(zr.File) > 1 {
		return nil, fmt.Errorf("more than one file found")
	}
	// Opening file
	rc, err := zr.File[0].Open()
	if err != nil {
		return nil, err
	}
	// Create a buffer to write the file content to.
	buf := new(bytes.Buffer)
	// Copying to buffer
	_, err = io.Copy(buf, rc)
	if err != nil {
		return nil, err
	}
	err = rc.Close()
	// Make sure to check the error on Close.
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
