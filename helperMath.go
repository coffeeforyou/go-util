package util

import "math"

// Returns the maximum value of the provided ints
func MaxNumber[T Number](nums ...T) T {
	number := nums[0]
	for i := len(nums) - 1; i > 0; i-- {
		if nums[i] > number {
			number = nums[i]
		}
	}
	return number
}

// Returns the minimum value of the provided ints
func MinNumber[T Number](nums ...T) T {
	number := nums[0]
	for i := len(nums) - 1; i > 0; i-- {
		if nums[i] < number {
			number = nums[i]
		}
	}
	return number
}

// Returns the maximum value of the provided ints
func MaxInt(nums ...int) int {
	return MaxNumber(nums...)
}

// Returns the minimum value of the provided ints
func MinInt(nums ...int) int {
	return MinNumber(nums...)
}

// Returns the sum of the provided numbers
func SumNumbers[T Number](nums ...T) T {
	res := nums[0]
	for i := len(nums) - 1; i > 0; i-- {
		res += nums[i]
	}
	return res
}

// Calculate logN with arbitrary base
func LogN(base float64, number float64) float64 {
	return math.Log(number) / math.Log(base)
}

// Returns the number rounded to two digits (e.g. 4.56)
func RoundOn2(number float64) float64 {
	return math.Round(number*100.0) / 100.0
}
