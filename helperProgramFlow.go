package util

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
	"time"
)

func ExitIfError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// Schedule calls function `f` with a period `p` offset by `o`. Run once immediately if immediate is set.
func Schedule(ctx context.Context, p time.Duration, o time.Duration, immediate bool, f func(time.Time)) time.Duration {
	// Position the first execution
	if immediate {
		f(time.Now())
	}
	first := time.Now().Truncate(p).Add(o)
	if first.Before(time.Now()) {
		first = first.Add(p)
	}
	waitFor := time.Until(first)
	firstC := time.After(waitFor)
	// Receiving from a nil channel blocks forever
	t := &time.Ticker{C: nil}
	repeat := func() {
		for {
			select {
			case v := <-firstC:
				// The ticker has to be started before f as it can take some time to finish
				t = time.NewTicker(p)
				f(v)
			case v := <-t.C:
				f(v)
			case <-ctx.Done():
				t.Stop()
				return
			}
		}
	}
	go repeat()
	return waitFor
}

func ExecuteCommand(command string, env []string, args ...string) (outStr string, errStr string, err error) {
	cmd := exec.Command(command, args...)
	cmd.Env = os.Environ()
	if len(env) > 0 {
		cmd.Env = append(cmd.Env, env...)
	}
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err = cmd.Run()
	outStr, errStr = stdout.String(), stderr.String()
	if err != nil {
		return outStr, errStr, fmt.Errorf("running command %s failed: %w", cmd, err)
	}
	return outStr, errStr, err
}

// Prints statistics about current memory usage
func PrintMemUsage() {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	// For info on each, see: https://golang.org/pkg/runtime/#MemStats
	fmt.Printf("Alloc = %v MiB", bToMb(m.Alloc))
	fmt.Printf("\tTotalAlloc = %v MiB", bToMb(m.TotalAlloc))
	fmt.Printf("\tSys = %v MiB", bToMb(m.Sys))
	fmt.Printf("\tNumGC = %v\n", m.NumGC)
}

// Converts a number of bytes in megabytes
func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}
