package util

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"image"
	"image/png"
	"net/http"
)

func EncodeImageAsDataUrl(img image.Image) (*string, error) {
	var buf bytes.Buffer
	bufwriter := bufio.NewWriter(&buf)
	err := png.Encode(bufwriter, img)
	if err != nil {
		return nil, err
	}
	bufwriter.Flush()
	bufbytes := buf.Bytes()
	var base64Encoding string
	// Determine the content type of the image file
	mimeType := http.DetectContentType(bufbytes)
	// Prepend the appropriate URI scheme header depending
	// on the MIME type
	switch mimeType {
	case "image/jpeg":
		base64Encoding += "data:image/jpeg;base64,"
	case "image/png":
		base64Encoding += "data:image/png;base64,"
	}
	// Append the base64 encoded output
	base64Encoding += base64.StdEncoding.EncodeToString(bufbytes)
	return &base64Encoding, nil
}
